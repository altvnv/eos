# EOSIO - Инфраструктура для децентрализованных приложений

EOSIO позволяет создавать и загружать высокопроизводительную, масштабируюмую блокчейн инфраструктуру, которой могут собираться децентрализованные приложения.

На данный момент код находится на этапе beta-тестирования. Это распространяется на тестовую сеть и смарт-контракты.

На данный момент еще нет публичной тестовой сети.

## Поддерживаемые ОС
EOSIO currently supports the following operating systems:  
1. Amazon 2017.09 и выше
2. Centos 7
3. Fedora 25 и выше (Fedora 27 рекомендуется)
4. Mint 18
5. Ubuntu 16.04 (Ubuntu 16.10 рекомендуется)
6. MacOS Darwin 10.12 и выше (MacOS 10.13.x рекомендуется)

# Ссылки
1. [eos.io Website](https://eos.io)
3. [EOSIO Blog](https://medium.com/eosio)
8. [EOSIO Documentation Wiki](https://github.com/EOSIO/eos/wiki)
2. [EOSIO API Documentation](https://eosio.github.io/eos/)
4. [Community Telegram Group](https://t.me/EOSProject)
5. [Developer Telegram Group](https://t.me/joinchat/EaEnSUPktgfoI-XPfMYtcQ)
6. [White Paper](https://github.com/EOSIO/Documentation/blob/master/TechnicalWhitePaper.md)
7. [Roadmap](https://github.com/EOSIO/Documentation/blob/master/Roadmap.md)

<a name="gettingstarted"></a>
## Приступаем к работе
Инструкция включает в себя процесс загрузки, сборки, запуска приватной тестовой сети, загрузки контрактов. Полную инструкцию и примеры смарт-контрактов можно прочитать на вики [Wiki](https://github.com/EOSIO/eos/wiki).
 
